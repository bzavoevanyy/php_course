<?php
Class Model_category extends Model
{
	protected $table = 'category';
	public $timestamps = false;
	protected $fillable = ['title', 'status'];
}