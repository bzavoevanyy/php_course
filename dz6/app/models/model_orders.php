<?php
Class Model_orders extends Model
{
    protected $table = 'orders';
    public $timestamps = false;
    protected $fillable = ['id_user', 'date_order', 'status'];
}