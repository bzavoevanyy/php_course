<?php

class View
{

    function generate($template, $table = null, $data = null, $access = [1, 1, 1, 1], $type = 'CSV')
    {
        require 'app/core/map.php';
        $loader = new Twig_Loader_Filesystem('app/views/templates');
        $twig = new Twig_Environment($loader, array('cache' => 'app/views/cache', 'debug' => 'true'));
        $twigdata = array('mainmenu' => $mainmenu, 'table' => $table, 'data' => $data, 'map' => $map, 'access' => $access, 'type' => $type);
        echo $twig->render($template, $twigdata);
    }
}