<!--Функция должна принимать в качестве аргумента массив чисел и возвращать-->
<!--так же массив, но отсортированный по возрастанию.-->
<!--Пример: В функцию передали [1, 22, 5, 66, 3, 57]. Вернула: [1, 3, 5, 22, 57, 66]-->
<?php
error_reporting(-1);
function sorter($numbers) {
    sort($numbers);
    return $numbers;
}
$numbers = [3,2,8,10,19,6,17,2,1,111];
echo join(', ',$numbers).'<br>';
echo join(', ',sorter($numbers));

echo '<br>';

// Сделать тоже самое, только без встроеной функции sort()

function sorterManual($numbers) {
    $new = array();
    foreach($numbers as $val) {
        $a = $numbers[0];
        $b = 0;
        foreach ($numbers as $key => $value) {
            if ($value < $a) {
                $a = $value;
                $b = $key;
            }
        }
        $new[] = $a;
        array_splice($numbers, $b, 1);
    }
    return $new;
}
echo  join(', ',sorterManual($numbers));