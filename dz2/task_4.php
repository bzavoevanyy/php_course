<!--Функция должна принимать два параметра – целые числа. Если в функцию-->
<!--переданы не целые числа, то функция должна выводить ошибку на экран и-->
<!--завершать работу. Если в функцию передали 2 целых числа, то функция-->
<!--должна отобразить таблицу умножения размером со значения параметров,-->
<!--переданных в функцию.-->
<!--Например: tabl(4,3), то функция должна нарисовать следующий результат:-->
<!--1 2 3 4-->
<!--2 4 6 8-->
<!--3 6 9 12-->
<?php
function tabl($a,$b) {
    if (is_int($a) && is_int($b)) {
        $first = true;
        $second = true;
        echo '<table style="border: 1px solid grey; border-collapse: separate;" >';
        for ($y = 0; $y <= $a; $y++) {
            echo '<tr>';
            if (!$first) echo '<td style="color: red;">' . $y . '</td>';
            for ($x = 1; $x <= $b; $x++) {
                if ($first) {
                    echo '<td>' . ' ' . '</td>';
                    echo '<td style="color: red">' . $x . '</td>';
                } elseif ($second) {
                    echo '<td style="color: red">' . $x . '</td>';
                } else echo '<td>' . $x*$y . '</td>';
                $first = false;
            }
            $second = false;
            echo '</tr>';
        }
        echo '</table>';
    } else echo 'Функция принимает только целые числа!!!';
}
echo '<p>Таблиа умножения</p>';
tabl(9,10);
/*
Сделать функцию, которая выводит таблицу квадратов чисел. В левом столбике - еденицы, в верхней строке - десятки
*/

function squareTable($a,$b) {
    if (is_int($a) && is_int($b)) {
        $first = true;
        $second = true;
        echo '<table style border="1px solid grey">';
        for ($y = -1; $y < $a; $y++) {
            echo '<tr>';
            if (!$first) echo '<td style="color: red;">' . $y . '</td>';
            for ($x = 0; $x < $b; $x++) {
                if ($first) {
                    echo '<td>' . ' ' . '</td>';
                    echo '<td style="color: red">' . $x . '</td>';
                } elseif ($second) {
                    echo '<td style="color: red">' . $x . '</td>';
                } else echo '<td>' . pow(intval($x.$y),2) . '</td>';
                $first = false;
            }
            $second = false;
            echo '</tr>';
        }
        echo '</table>';
    } else echo 'Функция принимает только целые числа!!!';
}
echo '<p>Таблиа квадратов</p>';
squareTable(9,9);
