<?php
/*
 Функция должна принимать массив строк и выводить каждую строку в
отдельном параграфе.
*/

function printArray($strings) {
    if (is_array($strings)) {
    foreach ($strings as $value) {
        echo '<p>'.$value.'</p>';
    }
    } else echo 'В функцию нужно передать массив';
}
$strings = ['Lorem', 'Foo', 'Bar', 'something'];
printArray($strings);

/* функция должна принимать массив строк, сортировать массив по алфавиту и выводить каждую строку в отдельном параграфе
*/

function printP($string) {
	if (is_array($string)) {
		asort($string);
		foreach ($string as $val) {
			echo '<p>'.$val.'<p>';
		}
	} else echo 'В функцию нужно передать массив';
}
echo '-------------------------';
printP($strings);
