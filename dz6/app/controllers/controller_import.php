<?php

class Controller_import extends Controller
{
    function saveToDB($newAr) {
            foreach ($newAr as $key => $value) {                          
                array_shift($value);
                try {
                    $temp = $this->model->firstOrNew($value);
                } catch (PDOException $e) {
                    print_r($e->getMessage());
                    $this->view->generate('main.html');
                }
                                       
                $temp->save(); 
                                              
            }
            $data = $this->model->all()->toArray();
            $this->view->generate($_POST['table'].'.html',$_POST['table'] , $data);
        }
    function action_index()
    {
        include 'app/core/map.php';
        switch ($_POST['table']) {
            case 'category':
                $this->model = new Model_category();
                break;
            case 'products':
                $this->model = new Model_products();
                break;
            case 'users':
                $this->model = new Model_users();
                break;
            case 'orders':
                $this->model = new Model_orders();
                break;
            case 'order_items':
                $this->model = new Model_order_items();
                break;

        }
        $tmp_name = $_FILES['file']['tmp_name'];
        $filename = $_FILES['file']['name'];
        $size = $_FILES['file']['size'];
        $type = $_FILES['file']['type'];
        
        
        if (is_uploaded_file($tmp_name)) {
            if ($type == 'text/csv' || $type == 'application/json' || $type == 'text/xml' || $type == 'application/octet-stream') {
                if ($size <= 2097152) {
                    if (move_uploaded_file($tmp_name, 'temp/' . $filename)) {
                        if ($type == 'text/csv') {
                            $handle = fopen('temp/'.$filename, 'r');
                            while (($temp = fgetcsv($handle,0,';', '"')) !== FALSE) {
                                $data[] = $temp;
                            }
                            $temp = array_shift($data);
                            foreach ($data as $key => $value) {
                                for ($i=0; $i < count($temp); $i++) { 
                                    $newAr[$key][$temp[$i]]= $value[$i];
                                }
                            }
                            $this->saveToDB($newAr);
                       }     
                        if ($type == 'application/json' || $type == 'application/octet-stream') {
                            $handle = fopen('temp/'.$filename, 'r');
                            $json = fread($handle, 2097152);
                            $newAr = json_decode($json,true);
                            $this->saveToDB($newAr);
                        }
                        if ($type == 'text/xml') {
                            $xml = simplexml_load_file('temp/'.$filename);
                            if ($_POST['table'] == 'category') {
                                $node = 'categories';
                            } else $node = $_POST['table'];
                            $elem = substr($node, 0, -1);
                            $i=0;                    
                            foreach ($xml->$elem as $key) {
                                
                                foreach ($map[$_POST['table']] as $k => $value) {
                                    $newAr[$i][$k] = $key->$k->__toString();
                                }
                                $i++;
                            }
                            $this->saveToDB($newAr);
                        }

                    }
                }
            }
        }
    }
}