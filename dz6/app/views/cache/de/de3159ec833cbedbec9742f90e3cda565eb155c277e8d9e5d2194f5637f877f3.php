<?php

/* index.html */
class __TwigTemplate_6ab92f111beeb9be82db603c6704fdcb233d82c9987f475fc8289c91f266a9d5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'menu' => array($this, 'block_menu'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"ru-RU\">
<head>
\t<meta charset=\"UTF-8\">
\t<meta name=viewport content=\"width=device-width, initial-scale=1\">
\t<link rel=\"stylesheet\" href=\"/css/bootstrap.css\">
\t<link rel=\"stylesheet\" href=\"/css/style.css\">
\t<title>";
        // line 8
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
</head>
<body>
\t<div class=\"wrapper\">
\t\t<div class=\"container-fluid\">
\t\t\t<div class=\"row\">
\t\t\t\t<ul class=\"nav nav-pills\">
\t\t\t\t\t";
        // line 15
        $this->displayBlock('menu', $context, $blocks);
        // line 18
        echo "\t\t\t\t</ul>\t
\t\t\t</div>
\t\t</div>
\t\t<div class=\"container-fluid\">
\t\t\t<div class=\"row\">
\t\t\t\t\t";
        // line 23
        $this->displayBlock('content', $context, $blocks);
        // line 26
        echo "\t\t\t</div>
\t\t</div>
\t</div>
\t<div class=\"footer\"></div>
</body>
<script src=\"https://code.jquery.com/jquery-1.12.1.min.js\"></script>
<script src=\"js/bootstrap.min.js\"></script>
</html>";
    }

    // line 8
    public function block_title($context, array $blocks = array())
    {
    }

    // line 15
    public function block_menu($context, array $blocks = array())
    {
        // line 16
        echo "
\t\t\t\t\t";
    }

    // line 23
    public function block_content($context, array $blocks = array())
    {
        // line 24
        echo "
\t\t\t\t\t";
    }

    public function getTemplateName()
    {
        return "index.html";
    }

    public function getDebugInfo()
    {
        return array (  79 => 24,  76 => 23,  71 => 16,  68 => 15,  63 => 8,  52 => 26,  50 => 23,  43 => 18,  41 => 15,  31 => 8,  22 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html lang="ru-RU">*/
/* <head>*/
/* 	<meta charset="UTF-8">*/
/* 	<meta name=viewport content="width=device-width, initial-scale=1">*/
/* 	<link rel="stylesheet" href="/css/bootstrap.css">*/
/* 	<link rel="stylesheet" href="/css/style.css">*/
/* 	<title>{%block title%}{%endblock%}</title>*/
/* </head>*/
/* <body>*/
/* 	<div class="wrapper">*/
/* 		<div class="container-fluid">*/
/* 			<div class="row">*/
/* 				<ul class="nav nav-pills">*/
/* 					{%block menu%}*/
/* */
/* 					{%endblock%}*/
/* 				</ul>	*/
/* 			</div>*/
/* 		</div>*/
/* 		<div class="container-fluid">*/
/* 			<div class="row">*/
/* 					{%block content%}*/
/* */
/* 					{%endblock%}*/
/* 			</div>*/
/* 		</div>*/
/* 	</div>*/
/* 	<div class="footer"></div>*/
/* </body>*/
/* <script src="https://code.jquery.com/jquery-1.12.1.min.js"></script>*/
/* <script src="js/bootstrap.min.js"></script>*/
/* </html>*/
