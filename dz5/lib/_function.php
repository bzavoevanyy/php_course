<?php
include('_config.php');
include('_map.php');
include('_conect.php');

function showPage($content)
{

    $template = file_get_contents('layout/_index.html');

    foreach ($content as $key => $value) {
        $template = str_replace("{%$key%}", $value, $template);
    }
    echo $template;
}

function mainmenu($mainmenu, $active)
{
    $html = '<ul class="nav nav-pills">';
    foreach ($mainmenu as $key => $value) {
        if ($key == $active) {
            $html .= '<li role="presentation" class="active"><a href="' . $value . '">' . $key . '</a></li>';
        } else {
            $html .= '<li role="presentation"><a href="' . $value . '">' . $key . '</a></li>';
        }
    }
    $html .= '</ul>';
    return $html;
}

function showTable($table, $data, $access = [1, 1, 1])
{
    $temp = $GLOBALS['map'][$table];
    $id = 0;

    $html = '<div class="container-fluid"><div class="row"><div class="col-lg-12"><table class="table table-hover"><thead><tr>';
    foreach ($temp as $key => $value) {
        $html .= '<th>' . $value . '</th>';
    }
    $html .= '</tr></thead><tbody>';
    foreach ($data as $key => $value) {
        $html .= '<tr>';
        foreach ($value as $k => $val) {
            $link = explode('_', $k);
            if ($link[0] == 'id' && !empty($link[1])) {
                if ($link[1] == 'catalog') $link[1] = 'category'; else $link[1] .= 's';
                $html .= '<td><a href="' . $link[1] . '.php?action=detail&id=' . $val . '">' . $val . '</a></td>';
            } else {
                $html .= '<td>' . $val . '</td>';
            }
            if ($k == 'id') $id = $val;
        }
        if ($access[0]) $html .= '<td><a href="' . $table . '.php?action=detail&id=' . $id . '">Подробнее</a></td>';
        if ($access[1]) $html .= '<td><a href="' . $table . '.php?action=edit&id=' . $id . '">Редактировать</a></td>';
        if ($access[2]) $html .= '<td><a href="' . $table . '.php?action=delete&id=' . $id . '">Удалить</a></td>';
        $html .= '</tr>';
        $id = 0;
    }
    $html .= '</tbody></table></div></div></div>';
    return $html;
}

function showSearch($table)
{
    global $map;
    $html = '<div class="container-fluid"><div class="row"><div class="col-lg-5"><form class="form-inline" action="' . $table . '.php" method="get">';
    $html .= '<input name = "action" value="search" hidden>';
    $html .= '<select name="field" class="form-control">';
    foreach ($map[$table] as $key => $value) {
        $html .= '<option value="' . $key . '">' . $value . '</option>';
    }
    $html .= '</select><input name="value" type="text" class="form-control" placeholder="Значение поля">';
    $html .= '<button type="submit" class="btn btn-primary">Искать</button></form></div></div></div>';
    return $html;
}

function showUpdate($table, $id)
{
    global $map;
    $html = '<div class="container-fluid"><div class="row"><div class="col-sm-6"><form class="form-inline" action="' . $table . '.php" method="get">';
    $html .= '<input name = "action" value="update" hidden>';
    $html .= '<div class="form-group"><label for="id" class="control-label">ID:</label><input class="form-control" id = "id" name = "id" value="' . $id . '" readonly></div>';
    $html .= '<div class="form-group"><select name="field" class="col-sm-1 form-control">';

    foreach ($map[$table] as $key => $value) {
        if ($key != 'id') {
            $html .= '<option value="' . $key . '">' . $value . '</option>';
        }
    }
    $html .= '</select></div><div class="form-group"><input name="value" type="text" class="form-control" placeholder="Значение поля"></div>';
    $html .= '<button type="submit" class="btn btn-primary">Изменить</button></form></div></div></div>';
    return $html;
}

function showFormNew($table)
{
    global $map;
    $html = '<div class="container-fluid"><div class="row"><div class="col-md-10"><form action="' . $table . '.php" method="get">';
    $temp = $map[$table];
    $html .= '<input name = "action" value="create" hidden>';
    foreach ($temp as $key => $value) {
        if ($key == 'id') {
            $html .= '<div class="form-group">
                    <label for="'.$key.'" class="col-sm-2 control-label">'.$value.'</label>
                    <div class="col-sm-10">
                    <input name = "'.$key.'" class="form-control" id="'.$key.'" readonly>
                    </div>
                    </div>';
        } else {


            $html .= '<div class="form-group">
                    <label for="' . $key . '" class="col-sm-2 control-label">' . $value . '</label>
                    <div class="col-sm-10">
                    <input name = "' . $key . '" class="form-control" id="' . $key . '">
                    </div>
                    </div>';
        }
    }
    $html .= '<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-default">Новая запись</button>
    </div>
  </div>';
    $html .= '</div></div></div></form>';
    return $html;
}

function showMessage($type, $message)
{
    if ($type == 'info') {
        $html = '<div class="alert alert-info" role="alert">' . $message . '</div>';
    } elseif ($type == 'error') {
        $html = '<div class="alert alert-danger" role="alert">' . $message . '</div>';
    } elseif ($type == 'success') {
        $html = '<div class="alert alert-success" role="alert">' . $message . '</div>';
    }
    return $html;
}