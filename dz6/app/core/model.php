<?php 
use Illuminate\Database\Capsule\Manager as Capsule;
$capsule = new Capsule;
$capsule->addConnection([
	    'driver'    => 'mysql',
	    'host'      => 'localhost',
	    'database'  => 'db_shop',
	    'username'  => 'root',
	    'password'  => 'root',
	    'charset'   => 'utf8',
	    'collation' => 'utf8_unicode_ci',
	    'prefix'    => '',
	]);
$capsule->bootEloquent();
class Model extends Illuminate\Database\Eloquent\Model {}