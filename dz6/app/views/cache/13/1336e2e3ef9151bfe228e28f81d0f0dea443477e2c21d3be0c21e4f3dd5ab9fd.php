<?php

/* content.html */
class __TwigTemplate_c7888384bf38753deb616ab286cdc5a117881d38d325d4771737a77c93aed202 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if (($this->getAttribute((isset($context["access"]) ? $context["access"] : null), 3, array(), "array") == 1)) {
            // line 2
            echo "<div class=\"container-fluid\">
    <div class=\"row\">
        <div class=\"col-lg-5\">
            <form class=\"form-inline\" action=\"/";
            // line 5
            echo twig_escape_filter($this->env, (isset($context["table"]) ? $context["table"] : null), "html", null, true);
            echo "/search\" method=\"post\">
                <select name=\"field\" class=\"form-control\">
                    ";
            // line 7
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["map"]) ? $context["map"] : null), (isset($context["table"]) ? $context["table"] : null), array(), "array"));
            foreach ($context['_seq'] as $context["key"] => $context["value"]) {
                // line 8
                echo "                    <option value=\"";
                echo twig_escape_filter($this->env, $context["key"], "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $context["value"], "html", null, true);
                echo "</option>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['key'], $context['value'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 10
            echo "                </select><input name=\"value\" type=\"text\" class=\"form-control\" placeholder=\"Значение поля\">
                <button type=\"submit\" class=\"btn btn-primary\">Искать</button>
            </form>
        </div>
    </div>
</div>
";
        }
        // line 17
        if (($this->getAttribute((isset($context["access"]) ? $context["access"] : null), 3, array(), "array") == 0)) {
            // line 18
            echo "<div class=\"container-fluid\">
    <div class=\"row\">
        <div class=\"col-sm-6\">
            <form class=\"form-inline\" action=\"/";
            // line 21
            echo twig_escape_filter($this->env, (isset($context["table"]) ? $context["table"] : null), "html", null, true);
            echo "/update\" method=\"post\">
                <div class=\"form-group\"><label for=\"id\" class=\"control-label\">ID:</label><input class=\"form-control\"
                                                                                                id=\"id\" name=\"id\"
                                                                                                value=\"";
            // line 24
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["data"]) ? $context["data"] : null), 0, array(), "array"), "id", array(), "array"), "html", null, true);
            echo "\"
                                                                                                readonly></div>
                <div class=\"form-group\"><select name=\"field\" class=\"col-sm-1 form-control\">
                    ";
            // line 27
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["map"]) ? $context["map"] : null), (isset($context["table"]) ? $context["table"] : null), array(), "array"));
            foreach ($context['_seq'] as $context["key"] => $context["value"]) {
                // line 28
                echo "                    ";
                if (($context["key"] != "id")) {
                    // line 29
                    echo "                    <option value=\"";
                    echo twig_escape_filter($this->env, $context["key"], "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $context["value"], "html", null, true);
                    echo "</option>
                    ";
                }
                // line 31
                echo "                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['key'], $context['value'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 32
            echo "                </select></div>
                <div class=\"form-group\"><input name=\"value\" type=\"text\" class=\"form-control\"
                                               placeholder=\"Значение поля\"></div>
                <button type=\"submit\" class=\"btn btn-primary\">Изменить</button>
            </form>
        </div>
    </div>
</div>
";
        }
        // line 41
        echo "<div class=\"container-fluid\">
    <div class=\"row\">
        <div class=\"col-lg-12\">
            <table class=\"table table-hover\">
                <thead>
                <tr>
                    ";
        // line 47
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["map"]) ? $context["map"] : null), (isset($context["table"]) ? $context["table"] : null), array(), "array"));
        foreach ($context['_seq'] as $context["key"] => $context["value"]) {
            // line 48
            echo "                    <th> ";
            echo twig_escape_filter($this->env, $context["value"], "html", null, true);
            echo "</th>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['value'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 50
        echo "                </tr>
                </thead>
                <tbody>
                ";
        // line 53
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["data"]) ? $context["data"] : null));
        foreach ($context['_seq'] as $context["key"] => $context["value"]) {
            // line 54
            echo "                <tr>
                    ";
            // line 55
            $context["id"] = 0;
            // line 56
            echo "                    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($context["value"]);
            foreach ($context['_seq'] as $context["k"] => $context["val"]) {
                // line 57
                echo "                    ";
                if (($context["k"] == "id")) {
                    $context["id"] = $context["val"];
                }
                // line 58
                echo "                    ";
                $context["foo"] = twig_split_filter($this->env, $context["k"], "_");
                // line 59
                echo "                    ";
                if ((($this->getAttribute((isset($context["foo"]) ? $context["foo"] : null), 0, array(), "array") == "id") && ($this->getAttribute((isset($context["foo"]) ? $context["foo"] : null), 1, array(), "array") != ""))) {
                    // line 60
                    echo "                    ";
                    if (($this->getAttribute((isset($context["foo"]) ? $context["foo"] : null), 1, array(), "array") == "catalog")) {
                        // line 61
                        echo "                    <td><a href=\"/category/findone/";
                        echo twig_escape_filter($this->env, $context["val"], "html", null, true);
                        echo "\">";
                        echo twig_escape_filter($this->env, $context["val"], "html", null, true);
                        echo "</a></td>
                    ";
                    } else {
                        // line 63
                        echo "                    <td><a href=\"/";
                        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["foo"]) ? $context["foo"] : null), 1, array(), "array"), "html", null, true);
                        echo "s/findone/";
                        echo twig_escape_filter($this->env, $context["val"], "html", null, true);
                        echo "\">";
                        echo twig_escape_filter($this->env, $context["val"], "html", null, true);
                        echo "</a></td>
                    ";
                    }
                    // line 65
                    echo "                    ";
                } else {
                    // line 66
                    echo "                    <td> ";
                    echo twig_escape_filter($this->env, $context["val"], "html", null, true);
                    echo "</td>
                    ";
                }
                // line 68
                echo "                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['k'], $context['val'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 69
            echo "                    ";
            if (($this->getAttribute((isset($context["access"]) ? $context["access"] : null), 0, array(), "array") == 1)) {
                // line 70
                echo "                    <td><a href=\"/";
                echo twig_escape_filter($this->env, (isset($context["table"]) ? $context["table"] : null), "html", null, true);
                echo "/findone/";
                echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : null), "html", null, true);
                echo "\">Подробнее</a></td>
                    ";
            }
            // line 72
            echo "                    ";
            if (($this->getAttribute((isset($context["access"]) ? $context["access"] : null), 1, array(), "array") == 1)) {
                // line 73
                echo "                    <td><a href=\"/";
                echo twig_escape_filter($this->env, (isset($context["table"]) ? $context["table"] : null), "html", null, true);
                echo "/edit/";
                echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : null), "html", null, true);
                echo "\">Редактировать</a></td>
                    ";
            }
            // line 75
            echo "                    ";
            if (($this->getAttribute((isset($context["access"]) ? $context["access"] : null), 2, array(), "array") == 1)) {
                // line 76
                echo "                    <td><a href=\"/";
                echo twig_escape_filter($this->env, (isset($context["table"]) ? $context["table"] : null), "html", null, true);
                echo "/delete/";
                echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : null), "html", null, true);
                echo "\">Удалить</a></td>
                    ";
            }
            // line 78
            echo "                </tr>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['value'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 80
        echo "                </tbody>
            </table>
        </div>
    </div>
</div>
<div class=\"container-fluid\">
    <div class=\"row\">
        <div class=\"col-md-10\">
            <form action=\"/";
        // line 88
        echo twig_escape_filter($this->env, (isset($context["table"]) ? $context["table"] : null), "html", null, true);
        echo "/create\" method=\"post\">
                ";
        // line 89
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["map"]) ? $context["map"] : null), (isset($context["table"]) ? $context["table"] : null), array(), "array"));
        foreach ($context['_seq'] as $context["key"] => $context["value"]) {
            // line 90
            echo "                ";
            if (($context["key"] == "id")) {
                // line 91
                echo "                <div class=\"form-group\">
                    <label for=\"";
                // line 92
                echo twig_escape_filter($this->env, $context["key"], "html", null, true);
                echo "\" class=\"col-sm-2 control-label\">";
                echo twig_escape_filter($this->env, $context["value"], "html", null, true);
                echo "</label>
                    <div class=\"col-sm-10\">
                        <input name=\"";
                // line 94
                echo twig_escape_filter($this->env, $context["key"], "html", null, true);
                echo "\" class=\"form-control\" id=\"";
                echo twig_escape_filter($this->env, $context["key"], "html", null, true);
                echo "\" readonly>
                    </div>
                </div>
                ";
            }
            // line 98
            echo "                ";
            if (($context["key"] != "id")) {
                // line 99
                echo "                <div class=\"form-group\">
                    <label for=\"";
                // line 100
                echo twig_escape_filter($this->env, $context["key"], "html", null, true);
                echo "\" class=\"col-sm-2 control-label\">";
                echo twig_escape_filter($this->env, $context["value"], "html", null, true);
                echo "</label>
                    <div class=\"col-sm-10\">
                        <input name=\"";
                // line 102
                echo twig_escape_filter($this->env, $context["key"], "html", null, true);
                echo "\" class=\"form-control\" id=\"";
                echo twig_escape_filter($this->env, $context["value"], "html", null, true);
                echo "\">
                    </div>
                </div>
                ";
            }
            // line 106
            echo "                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['value'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 107
        echo "                <div class=\"form-group\">
                    <div class=\"col-sm-offset-2 col-sm-10\">
                        <button type=\"submit\" class=\"btn btn-default\">Новая запись</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "content.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  301 => 107,  295 => 106,  286 => 102,  279 => 100,  276 => 99,  273 => 98,  264 => 94,  257 => 92,  254 => 91,  251 => 90,  247 => 89,  243 => 88,  233 => 80,  226 => 78,  218 => 76,  215 => 75,  207 => 73,  204 => 72,  196 => 70,  193 => 69,  187 => 68,  181 => 66,  178 => 65,  168 => 63,  160 => 61,  157 => 60,  154 => 59,  151 => 58,  146 => 57,  141 => 56,  139 => 55,  136 => 54,  132 => 53,  127 => 50,  118 => 48,  114 => 47,  106 => 41,  95 => 32,  89 => 31,  81 => 29,  78 => 28,  74 => 27,  68 => 24,  62 => 21,  57 => 18,  55 => 17,  46 => 10,  35 => 8,  31 => 7,  26 => 5,  21 => 2,  19 => 1,);
    }
}
/* {%if access[3] == 1%}*/
/* <div class="container-fluid">*/
/*     <div class="row">*/
/*         <div class="col-lg-5">*/
/*             <form class="form-inline" action="/{{table}}/search" method="post">*/
/*                 <select name="field" class="form-control">*/
/*                     {%for key, value in map[table]%}*/
/*                     <option value="{{key}}">{{value}}</option>*/
/*                     {%endfor%}*/
/*                 </select><input name="value" type="text" class="form-control" placeholder="Значение поля">*/
/*                 <button type="submit" class="btn btn-primary">Искать</button>*/
/*             </form>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* {%endif%}*/
/* {%if access[3]==0%}*/
/* <div class="container-fluid">*/
/*     <div class="row">*/
/*         <div class="col-sm-6">*/
/*             <form class="form-inline" action="/{{table}}/update" method="post">*/
/*                 <div class="form-group"><label for="id" class="control-label">ID:</label><input class="form-control"*/
/*                                                                                                 id="id" name="id"*/
/*                                                                                                 value="{{data[0]['id']}}"*/
/*                                                                                                 readonly></div>*/
/*                 <div class="form-group"><select name="field" class="col-sm-1 form-control">*/
/*                     {%for key, value in map[table]%}*/
/*                     {%if key != 'id'%}*/
/*                     <option value="{{key}}">{{value}}</option>*/
/*                     {%endif%}*/
/*                     {%endfor%}*/
/*                 </select></div>*/
/*                 <div class="form-group"><input name="value" type="text" class="form-control"*/
/*                                                placeholder="Значение поля"></div>*/
/*                 <button type="submit" class="btn btn-primary">Изменить</button>*/
/*             </form>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* {%endif%}*/
/* <div class="container-fluid">*/
/*     <div class="row">*/
/*         <div class="col-lg-12">*/
/*             <table class="table table-hover">*/
/*                 <thead>*/
/*                 <tr>*/
/*                     {%for key, value in map[table]%}*/
/*                     <th> {{value}}</th>*/
/*                     {%endfor%}*/
/*                 </tr>*/
/*                 </thead>*/
/*                 <tbody>*/
/*                 {%for key, value in data%}*/
/*                 <tr>*/
/*                     {%set id=0%}*/
/*                     {%for k, val in value%}*/
/*                     {%if k=='id'%}{%set id = val%}{%endif%}*/
/*                     {%set foo = k|split('_')%}*/
/*                     {%if foo[0]=='id' and foo[1]!='' %}*/
/*                     {%if foo[1] == 'catalog'%}*/
/*                     <td><a href="/category/findone/{{val}}">{{val}}</a></td>*/
/*                     {%else%}*/
/*                     <td><a href="/{{foo[1]}}s/findone/{{val}}">{{val}}</a></td>*/
/*                     {%endif%}*/
/*                     {%else%}*/
/*                     <td> {{val}}</td>*/
/*                     {%endif%}*/
/*                     {%endfor%}*/
/*                     {%if access[0] == 1%}*/
/*                     <td><a href="/{{table}}/findone/{{id}}">Подробнее</a></td>*/
/*                     {%endif%}*/
/*                     {%if access[1] == 1%}*/
/*                     <td><a href="/{{table}}/edit/{{id}}">Редактировать</a></td>*/
/*                     {%endif%}*/
/*                     {%if access[2] == 1%}*/
/*                     <td><a href="/{{table}}/delete/{{id}}">Удалить</a></td>*/
/*                     {%endif%}*/
/*                 </tr>*/
/*                 {%endfor%}*/
/*                 </tbody>*/
/*             </table>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* <div class="container-fluid">*/
/*     <div class="row">*/
/*         <div class="col-md-10">*/
/*             <form action="/{{table}}/create" method="post">*/
/*                 {%for key, value in map[table]%}*/
/*                 {%if key == 'id'%}*/
/*                 <div class="form-group">*/
/*                     <label for="{{key}}" class="col-sm-2 control-label">{{value}}</label>*/
/*                     <div class="col-sm-10">*/
/*                         <input name="{{key}}" class="form-control" id="{{key}}" readonly>*/
/*                     </div>*/
/*                 </div>*/
/*                 {%endif%}*/
/*                 {%if key != 'id'%}*/
/*                 <div class="form-group">*/
/*                     <label for="{{key}}" class="col-sm-2 control-label">{{value}}</label>*/
/*                     <div class="col-sm-10">*/
/*                         <input name="{{key}}" class="form-control" id="{{value}}">*/
/*                     </div>*/
/*                 </div>*/
/*                 {%endif%}*/
/*                 {%endfor%}*/
/*                 <div class="form-group">*/
/*                     <div class="col-sm-offset-2 col-sm-10">*/
/*                         <button type="submit" class="btn btn-default">Новая запись</button>*/
/*                     </div>*/
/*                 </div>*/
/*             </form>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* */
