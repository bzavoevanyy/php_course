<!--1. Создайте переменную $day и присвойте ей произвольное числовое значение-->
<!--2. С помощью конструкции switch выведите фразу “Это рабочий день”, если значение-->
<!--переменной $day попадает в диапазон чисел от 1 до 5 (включительно)-->
<!--3. Выведите фраху “Это выходной день”, если значение переменной $day равно-->
<!--числам 6 или 7-->
<!--4. Выведите фразу “Неизвестный день”, если значение переменной $day не попадает-->
<!--в диапазон чисел от 1 до 7 (включительно)-->
<!--5. Для закрепления изученного материала придумайте аналогичную задачу,-->
<!--желательно, немного усложнив ее. Напишите текст задачи и пример ее решения-->
<?php
$day = 7;
$workday = 'Это рабочий день<br>';
$holyday = 'Это выходной день<br>';
switch($day) {
    case 1:
        echo $workday;
        break;
    case 2:
        echo $workday;
        break;
    case 3:
        echo $workday;
        break;
    case 4:
        echo $workday;
        break;
    case 5:
        echo $workday;
        break;
    case 6:
        echo $holyday;
        break;
    case 7:
        echo $holyday;
        break;
    default:
        echo 'Неизвестный день<br>';
}

// Добавить в конструкцию переменную $time (утро, вечер) и выводить информацию "чем заняться" в зависимости от дня
// недели и времени
$day = 5;
$time = 'morning';
$workday = 'Это рабочий день<br>';
$holyday = 'Это выходной день<br>';
$workday_morning = 'Еще целый рабочий день впереди(<br>';
$workday_evening = 'Ура, можно немного отдохнуть!<br>';
$holyday_morning = 'Можно сходить погулять<br>';
$holyday_evening = 'Можно посмотреть кино<br>';
switch($day) {
    case 1:
        echo $workday;
        switch($time) {
            case 'morning':
                echo $workday_morning;
                break;
            case 'evening':
                echo $workday_evening;
                break;
        }
        break;
    case 2:
        echo $workday;
        switch($time) {
            case 'morning':
                echo $workday_morning;
                break;
            case 'evening':
                echo $workday_evening;
                break;
        }
        break;
    case 3:
        echo $workday;
        switch($time) {
            case 'morning':
                echo $workday_morning;
                break;
            case 'evening':
                echo $workday_evening;
                break;
        }
        break;
    case 4:
        echo $workday;
        switch($time) {
            case 'morning':
                echo $workday_morning;
                break;
            case 'evening':
                echo $workday_evening;
                break;
        }
        break;
    case 5:
        echo $workday;
        switch($time) {
            case 'morning':
                echo $workday_morning;
                break;
            case 'evening':
                echo $workday_evening;
                break;
        }
        break;
    case 6:
        echo $holyday;
        switch($time) {
            case 'morning':
                echo $holyday_morning;
                break;
            case 'evening':
                echo $holyday_evening;
                break;
        }
        break;
    case 7:
        echo $holyday;
        switch($time) {
            case 'morning':
                echo $holyday_morning;
                break;
            case 'evening':
                echo $holyday_evening;
                break;
        }
        break;
    default:
        echo 'Неизвестный день';
}
