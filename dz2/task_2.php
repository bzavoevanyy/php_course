<?php
/*Функция должна принимать 2 параметра: а) массив чисел; б) строку,
обозначающую арифметическое действие, которое нужно выполнить со
всеми элементами массива. Функция должна вывести результат.
Например: имя функции someFunction(array(1,2,3,4), ‘ – ’)
Результат: 1 – 2 – 3 – 4 = 8
*/
function counter($numbers, $oper) {
    $result =0;
    switch($oper) {
        case '-':
            $helper = true;
            foreach ($numbers as $val) {
                if ($helper) $result = $val;
                else $result = $result - $val;
                $helper = false;
            }
            echo join('-',$numbers).' = '.$result;
            break;
        case '+':
            $result = array_sum($numbers);
            echo join('+',$numbers).' = '.$result;
            break;
        case '*':
            $result = array_product($numbers);
            echo join('*',$numbers).' = '.$result;
            break;
        case '/':
            $helper = true;
            foreach ($numbers as $val) {
                if ($val == 0) { // Проверка на деление на ноль
                    echo 'Делить на ноль нельзя';
                    return;
                }
                if ($helper) $result = $val;
                else $result = $result / $val;
                $helper = false;
            }
            echo join('/',$numbers).' = '.$result;
            break;
    }
    return $result;
}
$numbers = [1,2,0,4];
$oper = '+';
counter($numbers, $oper);
echo '<p>Дополнительное задание</p>';

//В функцию нужно добавить проверку на деление на 0, функция должна возвращать результат вычислений и если он меньше
//определенного значения - в массив добавляется элемент - разница

$numbers = [1,2,3,4,5,6,7];
$min = 30;
$result = counter($numbers,$oper);
echo '<br>';
if ($result < $min) {
    $numbers[] = $min - $result;
    echo join($oper, $numbers). ' = '.$min;
}