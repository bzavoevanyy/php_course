<?php
session_start();
if (isset($_SESSION['name'])) {
    echo '<a href="logout.php">Выйти</a><br>';
} else {
    session_destroy();
    echo '<a href="loginform.html">Войти</a><br>';
    echo '<p>Вы не авторизованны!</p>';
    echo '<a href="index.php"> На главную</a>';
    die;
}
$table = array_shift($_POST);
$id = array_shift($_POST);
if (empty($id)) $id=null;
$data = $_POST;
if (!empty($_FILES)) {
    $filename = $_FILES['img']['name'];
    $tmp_name = $_FILES['img']['tmp_name'];
    $type = $_FILES['img']['type'];
    $size = $_FILES['img']['size'];
        if (is_uploaded_file($tmp_name)) {
            if ($type == 'text/plain' || $type == 'image/gif' || $type == 'image/png' || $type == 'image/jpeg') {
                if ($size <= 2097152 && !preg_match("/php/i",$filename)) {
                    if (move_uploaded_file($tmp_name, 'img/' . $filename)) $data['img'] = 'img/'.$filename;
                } else echo '<p>Файл слишком большой! В базу не занесен!</p>';
            } else echo '<p>Загружать можно только картинки!</p>';
        } else echo '<p>Ошибка загрузки!</p>';
}
function funcDie($table, $id) {
    echo 'Некоторые данные введены некорректно!<br>';
    echo '<a href="edit.php?table='.$table.'">Назад </a>';
    die();
}
require ('var.php');
try {
    $dbh = new PDO("mysql:host=".$host.";dbname=db_shop",$mysqllogin,$mysqlpassword);
} catch (PDOException $e) {

}
if ($id) {
    $sql = 'update ' . $table . ' set ';
} else {
    $sql = 'insert into '. $table .'(id,';
}
$types = $dbh->query('desc '.$table, PDO::FETCH_NUM)->fetchAll();
$i=1;
$valarray = [];
foreach ($data as $key => $val) {
    $temp = explode('(',$types[$i][1]);
    switch ($temp[0]) {
        case 'int':
            $val += 0;
            if ($val>0) {
                if ($id) {
                    $sql .= $key . ' = ' . $val . ',';
                } else {
                    $sql .= $key . ',';
                    $valarray[] = $val;
                }
            } else funcDie($table,$id);
            break;
        case 'varchar':
            if (is_string($val) && strlen($val)<=255) {
                if ($id) {
                    $sql .= $key . ' = "' . htmlentities($val) . '",';
                } else {
                    $sql .= $key . ',';
                    $valarray[] = $val;
                }
            } else funcDie($table,$id);
            break;
        case 'text':
            if (is_string($val)) {
                if ($id) {
                    $sql .= $key . ' = "' . htmlentities($val) . '",';
                } else {
                    $sql .= $key . ',';
                    $valarray[] = $val;
                }
            } else funcDie($table,$id);
            break;
        case 'tinytext':
            if (is_string($val)) {
                if ($id) {
                    $sql .= $key . ' = "' . $val . '",';
                } else {
                    $sql .= $key . ',';
                    $valarray[] = $val;
                }
            } else funcDie($table,$id);
            break;
        case 'date':
            $date = explode('-',$val);
            if (checkdate($date[1],$date[2],$date[0])) {
                if ($id) {
                    $sql .= $key . ' = "' . $val . '",';
                } else {
                    $sql .= $key . ',';
                    $valarray[] = $val;
                }
            } else funcDie($table,$id);
            break;
        case 'enum':
            if ($val == 'active' || $val == 'passive') {
                if ($id) {
                    $sql .= $key . ' = "' . $val . '",';
                } else {
                    $sql .= $key . ',';
                    $valarray[] = $val;
                }
            } else funcDie($table,$id);
            break;
        default:
            if ($id) {
                $sql .= $key . ' = "' . $val . '",';
            } else {
                $sql .= $key . ',';
                $valarray[] = $val;
            }
            break;
    }
    $i++;
}
$sql = rtrim($sql,',');
if ($id) {
    $sql .= ' where id = '.$id;
} else {
    $sql .= ') values (null, ';
    foreach ($valarray as $val) {
        $sql .= '"'.$val.'",';
    }
    $sql = rtrim($sql,',');
    $sql .= ')';
}
$q = $dbh->query($sql);
echo 'Запись сохранена!<br>';
echo '<a href="edit.php?table='.$table.'&id='.$id.'">Назад </a>';
