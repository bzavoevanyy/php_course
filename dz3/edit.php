<!-- Включаем сессию -->
<?php
session_start();
$file = $_GET['filename'];
$dir = 'files/'.$file;
$flag = $_GET['flag'];
?>
<!doctype html>
<html lang="ru-RU" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/style.css">
    <title>Document</title>
</head>
<body>
<div class="wrapper">
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">TheBest CMS</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
<!--            Скрипт проеверяет - если пользователь авторизирован, выводит имя пользователя и кнопку "Выйти",-->
<!--            если нет - кнопку "Войти"-->
                <?php
                if (isset($_SESSION['name'])) {
                    echo '<p class="navbar-text">Signed in as '.$_SESSION["name"].'</p>
            <a href="logout.php" type="button" class="btn btn-default navbar-btn">Sign out</a>';
                } else {
                    echo '<a href="login.php" type="button" class="btn btn-default navbar-btn">Sign in</a>';
                }
                ?>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
    <!--    В следующем скрипте выводим сайдбар если пользователь авторизирован-->
    <?php
    echo '<div class="container-fluid"><div class="row">';
    if (isset($_SESSION['name'])) {
        echo '<div class="col-md-2">
        <ul class="nav nav-pills nav-stacked">';
        if (!$flag) echo '<li role="presentation" class="active"><a href="edit.php?flag=0">Создать файл</a></li>';
            else echo '<li role="presentation"><a href="edit.php?flag=0">Создать файл</a></li>';
            echo '<li role="presentation"><a href="uploadform.php">Загрузить файл</a></li>
        </ul>
    </div>';};
    ?>
    <?php
    if (!isset($_SESSION['name'])) {
        echo '<div class="container"><div class="row"><p>Вы не авторизованы</p><a class="btn btn-default" href="index.php">На главную</a></div></div>';
        session_destroy();
    } else {
//        Если пользователь авторизован - выводим форму редактора
        echo '<div class="col-md-10">';
        echo '<form class="form-horizontal" method="post" action="save.php">';
        if (!$flag) { // Форма создания файла - можно менять имя файла
            echo '<div class="form-group">
                    <label class="col-sm-2 control-label">Имя файла</label>
                    <div class="col-sm-10">
                    <textarea name="filename" rows="1" class="form-control"></textarea>
                    </div></div>';
            echo '<div class="form-group"><label for="text" class="col-sm-2 control-label">Текст</label><div class="col-sm-10"><textarea id="text" name="text" class="form-control" rows="10">';
            echo '</textarea></div></div>';
        } else { // Форма редактирования файла - имя файла внесено и менять нельзя
            echo '<div class="form-group">
                    <label class="col-sm-2 control-label">Имя файла</label>
                    <div class="col-sm-10">
                    <textarea name="filename" rows="1" class="form-control" readonly>'.$file.'</textarea>
                    </div></div>';
            echo '<div class="form-group"><label for="text" class="col-sm-2 control-label">Текст</label><div class="col-sm-10"><textarea id="text" name="text" class="form-control" rows="10">';
            if (!@readfile($dir)) echo 'Ошибка! Не могу прочитать файл!'; // Загружаем текст из файла
            echo '</textarea></div></div>';
        }
        echo '<div class="col-sm-10 col-sm-offset-2"><button type="submit" class="btn btn-success">Сохранить</button><a class="btn btn-default" href="index.php" role="button" style="margin-left: 10px;">Отмена</a></div>';
        echo '</div>';
    }
    ?>
</div>
</div>
<div class="empty"></div>
</div>
<footer>
    <div class="container">
        <div class="row">
            <span class="footer">&#169 The Best CMS. Bogdan Zavoevanyy LS PHP 02-16</span>
        </div>
    </div>
</footer>
</body>
</html>
