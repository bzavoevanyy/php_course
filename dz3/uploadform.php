<?php
session_start();
?>
<!doctype html>
<html lang="ru-RU" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/style.css">
    <title>Document</title>
</head>
<body>
<div class="wrapper">
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">TheBest CMS</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
<!--            Скрипт проеверяет - если пользователь авторизирован, выводит имя пользователя и кнопку "Выйти",-->
<!--            если нет - кнопку "Войти"-->
                <?php
                if (isset($_SESSION['name'])) {
                    echo '<p class="navbar-text">Signed in as '.$_SESSION["name"].'</p>
                        <a href="logout.php" type="button" class="btn btn-default navbar-btn">Sign out</a>';
                } else {
                    echo '<a href="login.php" type="button" class="btn btn-default navbar-btn">Sign in</a>';
                }
                ?>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
    <!--    В следующем скрипте выводим сайдбар если пользователь авторизирован-->
    <?php
    echo '<div class="container-fluid"><div class="row">';
    if (isset($_SESSION['name'])) {
        echo '<div class="col-md-2">
    <ul class="nav nav-pills nav-stacked">';
        echo '<li role="presentation"><a href="edit.php?flag=0">Создать файл</a></li>';
        echo '<li role="presentation" class="active"><a href="uploadform.php">Загрузить файл</a></li></ul></div>';
    };
    ?>
    <?php
    if (!isset($_SESSION['name'])) {
        echo '<div class="container"><div class="row"><p>Вы не авторизованы</p><a class="btn btn-default" href="index.php">На главную</a></div></div>';
        session_destroy();
    } else {
//        Если пользователь авторизован - выводим форму загрузки файла
        echo '<div class="col-md-9 col-md-offset-1">';
        echo '
                <form class="form-horizontal" method="post" action="upload.php" enctype="multipart/form-data">
                <div class="form-group">
                <label for="exampleInputFile">File input</label>
                <input type="file" name= "filename" id="exampleInputFile">
                </div>
                <div class="form-group">

          <button type="submit" class="btn btn-success">Загрузить</button>
          <a class="btn btn-default" href="index.php" role="button">Отмена</a>
        </div>

    </form>
        </div>';
    }
    ?>
</div>
</div>
<div class="empty"></div>
</div>
<footer>
    <div class="container">
        <div class="row">
            <span class="footer">&#169 The Best CMS. Bogdan Zavoevanyy LS PHP 02-16</span>
        </div>
    </div>
</footer>
<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
</body>
</html>


