<?php
Class Model_users extends Model
{
    protected $table = 'users';
    public $timestamps = false;
    protected $fillable = ['name', 'lastname', 'birthday', 'email', 'password', 'is_active', 'reg_date', 'last_update', 'status'];
}