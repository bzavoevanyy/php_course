<!--Функция должна принимать переменное число аргументов, но первым-->
<!--аргументом обязательно должна быть строка, обозначающая арифметическое-->
<!--действие, которое необходимо выполнить со всеми передаваемыми-->
<!--аргументами. Остальные аргументы целые и/или вещественные.-->
<!--Например: имя функции someFunction(‘+’, 1, 2, 3, 5.2);-->
<!--Результат: 1 + 2 + 3 + 5.2 = 11.2-->
<?php
/*
 * Добавить возможность возведения в степень, но только если аргументов не больше двух
 */
function counter($math) {
    $numbers = func_get_args();
    $oper = array_shift($numbers);
    switch($oper) {
        case '-':
            $helper = true;
            $result = 0;
            foreach ($numbers as $val) {
                if ($helper) $result = $val;
                else $result = $result - $val;
                $helper = false;
            }
            echo join('-',$numbers).' = '.$result;
            break;
        case '+':
            echo join('+',$numbers).' = '.array_sum($numbers);
            break;
        case '*':
            echo join('*',$numbers).' = '.array_product($numbers);
            break;
        case '/':
            $helper = true;
            $result = 0;
            foreach ($numbers as $val) {
                if ($helper) $result = $val;
                else $result = $result / $val;
                $helper = false;
            }
            echo join('/',$numbers).' = '.$result;
            break;
        case '**':
            if (func_num_args() == 3) {
                echo join('**',$numbers).' = '.$numbers[0]**$numbers[1];
            } else echo 'Слишком много аргументов';
            break;
    }
}
counter('+',10,4,3,2,4,5);

