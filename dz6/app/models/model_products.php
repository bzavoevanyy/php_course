<?php
Class Model_products extends Model
{
    protected $table = 'products';
    public $timestamps = false;
    protected $fillable = ['id_catalog','title', 'mark','count','price', 'descriptions','status','img'];
}