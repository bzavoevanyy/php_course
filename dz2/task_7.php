<?php
/*
Функция принимает 1 строковый параметр и возвращает TRUE, если строка
является палиндромом, FALSE в противном случае.
Палиндром – строка, одинаково читающаяся в обоих направлениях.
 */

// Функция разворачивания строки в многобайтной кодировке
function mb_strrev($str, $encoding='UTF-8'){
    return mb_convert_encoding( strrev( mb_convert_encoding($str, 'UTF-16BE', $encoding) ), $encoding, 'UTF-16LE');
}

function is_polidrom($string) {
    $string = mb_convert_case($string,MB_CASE_LOWER); // изменяем регистр всех символов
    $string = mb_ereg_replace(" ", "", $string); // удаляем пробелы
    $polidrom = mb_strrev($string); // Развернули строку
    $chars = mb_strlen($string);
    for ($i=0;$i<$chars;$i++) {
        if (mb_substr($polidrom,$i,1) != mb_substr($string,$i,1)) return false;
    }
    return true;
}
$string = 'А роза упала на лапу Азора';
if (is_polidrom($string)) echo $string.' - это палидром';
// Добавить возможность определять длинные фразы-палидромы с пробелами и разным регистром символов