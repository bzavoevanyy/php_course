<?php
require_once "../vendor/autoload.php";
$faker = Faker\Factory::create();
$host = $_POST['host'];
$mysqllogin = $_POST['mysqllogin'];
$mysqlpassword = $_POST['mysqlpassword'];
$handle = fopen('var.php', 'w+');
if (fwrite($handle, '<?php $host="'.$host.'"; $mysqllogin="'.$mysqllogin.'"; $mysqlpassword="'.$mysqlpassword.'";')) {
    echo '<p>Файл с настройками успешно сохранен!</p>';
} else {
    echo '<p>Произошла ошибка сервера! Не могу создать файл var.php - проверьте права на директорию</p>';
    die;
}
require ('var.php');
try {
    $dbh = new PDO("mysql:host=".$host.";dbname=db_shop",$mysqllogin,$mysqlpassword);
} catch (PDOException $e) {
    if ($e->getCode() == 1049) {
        $dbh = new PDO("mysql:host=".$host,$mysqllogin,$mysqlpassword);
        $sql = 'create database db_shop CHARACTER SET \'utf8\'';
        $dbh->query($sql);
        $dbh->query('CREATE TABLE `db_shop`.`category` ( `id` INT NOT NULL AUTO_INCREMENT , `title` VARCHAR(255) NOT NULL , `status` ENUM(\'active\',\'passive\',\'\',\'\') NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;');
        $dbh->query('CREATE TABLE `db_shop`.`products` ( `id` INT NOT NULL AUTO_INCREMENT , `id_catalog` INT NOT NULL , `title` VARCHAR(255) NOT NULL , `mark` VARCHAR(255) NOT NULL , `count` INT NOT NULL , `price` FLOAT NOT NULL , `description` TEXT NOT NULL , `status` ENUM(\'active\',\'passive\',\'\',\'\') NOT NULL , `img` TINYTEXT NOT NULL, PRIMARY KEY (`id`)) ENGINE = InnoDB;');
        $dbh->query('CREATE TABLE `db_shop`.`users` ( `id` INT NOT NULL AUTO_INCREMENT , `name` VARCHAR(255) NOT NULL , `lastname` VARCHAR(255) NOT NULL , `birthday` DATE NOT NULL , `email` VARCHAR(255) NOT NULL , `password` VARCHAR(255) NOT NULL , `is_active` BOOLEAN NOT NULL , `reg_date` DATE NOT NULL , `last_update` DATE NOT NULL , `status` ENUM(\'active\',\'passive\',\'\',\'\') NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;');
        $dbh->query('CREATE TABLE `db_shop`.`orders` ( `id` INT NOT NULL AUTO_INCREMENT , `id_user` INT NOT NULL , `date_order` DATE NOT NULL , `status` ENUM(\'active\',\'passive\',\'\',\'\') NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;');
        $dbh->query('CREATE TABLE `db_shop`.`order_items` ( `id` INT NOT NULL AUTO_INCREMENT , `id_order` INT NOT NULL , `id_product` INT NOT NULL , `price` FLOAT NOT NULL , `count` INT NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;');
        $dbh = new PDO("mysql:host=".$host.";dbname=db_shop",$mysqllogin,$mysqlpassword);
    }
    echo '<p>База данных db_shop создана! Таблицы добавлены</p>';
}
$stmt = $dbh->prepare('INSERT INTO `db_shop`.`category` (id,title,status) VALUES (NULL,:title,:status)');
$stmt->bindParam(':title', $title);
$stmt->bindParam(':status', $status);
$status = 'active';
for ($i=0;$i<10;$i++) {
    $title = $faker->word;
    $stmt->execute();
}
$stmt = null;
$stmt = $dbh->prepare('INSERT INTO `db_shop`.`products` (id,id_catalog,title,mark,count,price,description,status,img) VALUES (NULL,:id_catalog,:title,:mark,:count,:price,:description,:status,:img)');
$stmt->bindParam(':id_catalog',$id_catalog);
$stmt->bindParam(':title',$title);
$stmt->bindParam(':mark',$mark);
$stmt->bindParam(':count',$count);
$stmt->bindParam(':price',$price);
$stmt->bindParam(':description',$description);
$stmt->bindParam(':status',$status);
$stmt->bindParam(':img',$img);
$status = 'active';
$img = 'img/cooltech.png';
for ($i=0;$i<10;$i++) {
    $id_catalog = $faker->numberBetween(1,10);
    $title = $faker->word;
    $mark = $faker->word;
    $count = $faker->numberBetween(10,100);
    $price = $faker->randomFloat(2,100,5000);
    $description = $faker->text(200);
    $stmt->execute();
}
$stmt = null;
$stmt = $dbh->prepare('INSERT INTO `db_shop`.`users` (id,name,lastname,birthday,email,password,is_active, reg_date, last_update, status) VALUES (NULL,:name,:lastname,:birthday,:email,:password,:is_active, :reg_date, :last_update, :status)');
$stmt->bindParam(':name',$name);
$stmt->bindParam(':lastname',$lastname);
$stmt->bindParam(':birthday',$birthday);
$stmt->bindParam(':email',$email);
$stmt->bindParam(':password',$password);
$stmt->bindParam(':is_active',$is_active);
$stmt->bindParam(':reg_date',$reg_date);
$stmt->bindParam(':last_update',$last_update);
$stmt->bindParam(':status',$status);
$status = 'active';
$is_active = 1;
for ($i=0;$i<10;$i++) {
    $name = $faker->firstName;
    $lastname = $faker->lastName;
    $birthday = $faker->dateTimeBetween('1970-01-01', '2000-01-01')->format('Y-m-d');
    $email = $faker->email;
    $password = $faker->password;
    $reg_date = $faker->dateTimeBetween('2005-01-01', '2014-01-01')->format('Y-m-d');
    $last_update = $faker->dateTimeBetween('2015-01-01', '2016-02-01')->format('Y-m-d');
    $stmt->execute();
    
}
$stmt = null;
$stmt = $dbh->prepare('INSERT INTO `db_shop`.`orders` (id,id_user,date_order,status) VALUES (NULL,:id_user,:date_order,:status)');
$stmt->bindParam(':id_user',$id_user);
$stmt->bindParam(':date_order',$date_order);
$stmt->bindParam(':status',$status);
$status = 'active';
for ($i=0;$i<10;$i++) {
    $id_user = $faker->unique()->numberBetween(1,10);
    $date_order = $faker->dateTimeBetween('2016-01-01', '2016-02-25')->format('Y-m-d');
    $stmt->execute();
    
}
$stmt = null;
$faker->unique(true);
$stmt = $dbh->prepare('INSERT INTO `db_shop`.`order_items` (id,id_order,id_product,price,count) VALUES (NULL,:id_order,:id_product,:price,:count)');

$stmt->bindParam(':id_order',$id_order);
$stmt->bindParam(':id_product',$id_product);
$stmt->bindParam(':price',$price);
$stmt->bindParam(':count',$count);

for ($i=1;$i<11;$i++) {
    $id_order = $i;
    for ($y=1;$y<4;$y++) {
    $id_product = $faker->unique()->numberBetween(1,10);
        $price = $dbh->query('select price from products where id='.$id_product)->fetchAll(PDO::FETCH_NUM)[0][0];
    $count = 2;
    $stmt->execute();
   
}
$faker->unique(true);
}
echo '<p>Таблицы заполненны временными данными!</p>';
echo '<a href="index.php">На главную</a>';
 