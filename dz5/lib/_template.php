<?php
if ($action == '')
{
    $content['content'] .= showSearch($table);
    $sth = $obj->find();
    $access = [1, 1, 1];
    $content['content'] .= showTable($table, $sth, $access);
    $content['content'] .= showFormNew($table);
} elseif ($action == 'detail')
{
    $content['content'] .= showUpdate($table,$id);
    $sth = $obj->findOne($id);
    if (empty($sth)) {
        $content['content'] .= showMessage('error', 'Категория не найдена!');
    } else {
        $access = [0, 1, 1];
        $content['content'] .= showTable($table, $sth, $access);
    }

} elseif ($action == 'search')

{
    $sth = $obj->findBy(array($field => $value));
    if (empty($sth)) {
        $content['content'] .= showMessage('error', 'Категория не найдена!');
    } else {
        if (count($sth) > 1) $access = [1, 1, 1]; else {
            $access = [0, 1, 1];
            $content['content'] .= showUpdate($table,$sth[0]['id']);
        }

        $content['content'] .= showTable($table, $sth, $access);
    }

} elseif ($action == 'edit')

{
    $content['content'] .= showMessage('info', 'В разработке');

} elseif ($action == 'update') {

    $sth = $obj->update(array ('id' => $id), array($field => $value));

    if ($sth[0] == 0) {
        $sth = $obj->find();
        $access = [1, 1, 1];
        $content['content'] = showSearch($table);
        $content['content'] .= showTable($table, $sth, $access);
        $content['content'] .= showFormNew($table);
    } else {
        $content['content'] .= showMessage('error', $sth[2]);
    }


} elseif ($action == 'delete')

{
    $sth = $obj->delete($id);
    if ($sth[0] == 0) {
        $sth = $obj->find();
        $access = [1, 1, 1];
        $content['content'] .= showSearch($table);
        $content['content'] .= showTable($table, $sth, $access);
        $content['content'] .= showFormNew($table);
    } else {
        $content['content'] .= showMessage('error', $sth[2]);
    }
} elseif ($action == 'create') {

    $temp = $_GET;
    array_shift($temp);
    foreach ($temp as $key => $value) {
        $obj->$key = $value;
    }
    $sth = $obj->create();
    if ($sth[0] == 0) {
        $sth = $obj->find();
        $access = [1, 1, 1];
        $content['content'] .= showSearch($table);
        $content['content'] .= showTable($table, $sth, $access);
        $content['content'] .= showFormNew($table);
    } else {
        $content['content'] .= showMessage('error', $sth[2]);
    }

}

showPage($content);

