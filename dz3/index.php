<!-- Включаем сессию -->
<?php
session_start();
?>
<!doctype html>
<html lang="ru-RU">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/style.css">
    <title>Document</title>
</head>
<body>
<div class="wrapper">
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php">TheBest CMS</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
<!--            Скрипт проеверяет - если пользователь авторизирован, выводит имя пользователя и кнопку "Выйти",-->
<!--            если нет - кнопку "Войти"-->
            <?php
                if (isset($_SESSION['name'])) {
                    echo '<p class="navbar-text">Signed in as '.$_SESSION["name"].'</p>
                    <a href="logout.php" type="button" class="btn btn-default navbar-btn">Sign out</a>';
                } else {
                    echo '<a href="login.php" type="button" class="btn btn-default navbar-btn">Sign in</a>';
                }
            ?>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
<!--    В следующем скрипте выводим сайдбар если пользователь авторизирован-->
<?php
echo '<div class="container-fluid"><div class="row">';
if (isset($_SESSION['name'])) {
    echo '<div class="col-md-2">
                <ul class="nav nav-pills nav-stacked">
                    <li role="presentation"><a href="edit.php?flag=0">Создать файл</a></li>
                    <li role="presentation"><a href="uploadform.php">Загрузить файл</a></li>
                </ul>
            </div>';};
?>
<?php
$dir = 'files';
$files = scandir($dir);
//Далее - функция вывода списка файлов
function showfile($files) {
    $finfo = finfo_open(FILEINFO_MIME_TYPE); // для определения mime типа файла
    echo '
        <div class="col-md-10">
            <table class="table table-hover">
                <tr>
                    <th>#</th>
                    <th>Имя файла</th>

                </tr>
                <tbody>';
//    Собственно, вывод файла в зависимости от его типа
    foreach ($files as $key => $val) {
        if ($val!='..' && $val!='.') {
            $mime = finfo_file($finfo, $GLOBALS['dir'].'/'.$val);
            if (isset($_SESSION['name'])) {
                echo '<tr><td>'.($key-1).'</td><td><a href="read.php?filename=' . $val . '">' . $val.'</a></td>';
                if ($mime == 'text/plain') {
                    echo '<td><a href="edit.php?filename=' . $val . '&flag=1">Редактировать</a></td>';

                } else echo '<td>'.$mime.'</td>';
                echo '<td><a href="download.php?filename=' . $val . '">Скачать</a></td>';
                echo '<td><a href="delete.php?filename=' . $val . '">Удалить</a></td></tr>';
            } else {
                echo '<tr><td>'.($key-1).'</td><td>'.$val.'</td></tr>';
            }
        }
    }
    finfo_close($finfo);
    echo '</tbody></table></div>';
}
showfile($files);
?>
</div>
</div>
<div class="empty"></div>
</div>
<footer>
    <div class="container">
        <div class="row">
            <span class="footer">&#169 The Best CMS. Bogdan Zavoevanyy LS PHP 02-16</span>
        </div>
    </div>
</footer>
</body>
</html>
