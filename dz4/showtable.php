<?php
session_start();
if (isset($_SESSION['name'])) {
    echo '<a href="logout.php">Выйти</a><br>';
} else {
    echo '<a href="loginform.html">Войти</a><br>';
}
$table = $_GET['table'];
$param = $_GET['param'];
require ('var.php');
try {
    $dbh = new PDO("mysql:host=".$host.";dbname=db_shop",$mysqllogin,$mysqlpassword);
} catch (PDOException $e) {

}
echo '<p>База данных db_shop, таблица '.$table.'</p>';
if (!isset($param)) {
    $sql = 'select * from ' . $table . ' limit 50';
} else {
    $sql = 'select * from ' . $table . ' where id='.$param.' limit 50';
}
$q = $dbh->query($sql,PDO::FETCH_ASSOC);
$string = $q->fetchAll();
echo '<table style="border-collapse: collapse;"><tr>';
foreach ($string[0] as $key=>$value) {
        echo '<th>'.$key.'</th>';
}
echo '</tr><tbody>';
foreach ($string as $key=>$value) {
    echo '<tr>';

    foreach($value as $item => $val) {
        if ($item == 'id') {
            $id = $val;
        }
        $temp = explode('_', $item, 2);
        if ($temp[1] && $temp[0] == 'id') {
            if ($temp[1] == 'catalog') {
                array_splice($temp,1,1,'category');
            } else $temp[1] .= 's';
            echo '<td style="border: 1px solid grey;"><a href="showtable.php?table='.$temp[1].'&param='.$val.'"</a>'. $val . '</td>';
        } elseif ($temp[0] == 'img' && !empty($param)) {
            echo '<td style="border: 1px solid grey;"><img style="max-width: 200px;" src="'.$val.'"></td>';
        } else echo '<td style="border: 1px solid grey;">'.$val.'</td>';
    }
    echo '<td style="border: 1px solid grey;"><a href="showtable.php?table='.$table.'&param='.$id.'">Просмотр</a></td>';
    if (isset($_SESSION['name'])) {
        echo '<td style="border: 1px solid grey;"><a href="edit.php?table=' . $table . '&id=' . $id . '">Редактировать</a></td>';
        echo '<td style="border: 1px solid grey;"><a href="delete.php?table=' . $table . '&id=' . $id . '">Удалить</a></td>';
    }
    echo '</tr>';
}

echo '</tbody></table>';
if (isset($_SESSION['name'])) {
    echo '<a href="edit.php?table='.$table.'">Добавить запись </a>';
}
echo '<a href="index.php">На главную</a>';
