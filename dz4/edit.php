<?php
session_start();
if (isset($_SESSION['name'])) {
    echo '<a href="logout.php">Выйти</a><br>';
} else {
    session_destroy();
    echo '<a href="loginform.html">Войти</a><br>';
    echo '<p>Вы не авторизованны!</p>';
    echo '<a href="index.php"> На главную</a>';
    die;
}
$table = $_GET['table'];
$id = $_GET['id'];
require ('var.php');
try {
    $dbh = new PDO("mysql:host=".$host.";dbname=db_shop",$mysqllogin,$mysqlpassword);
} catch (PDOException $e) {
    echo 'Ошибка!';
}
if ($id) {
    $sth = $dbh->query('select * from ' . $table . ' where id=' . $id)->fetchAll(PDO::FETCH_NUM)[0];
} else $sth=null;
$types = $dbh->query('desc '.$table)->fetchAll(PDO::FETCH_NUM);
$i=1;
echo '<form action="save.php" method="post" enctype="multipart/form-data">';
echo '<input type="hidden" name="table" value="'.$table.'">';

foreach ($types as $key => $value) {
    if ($id)  $inputval = $sth[$key]; else $inputval = null;
    if (!$key) $dis = 'readonly'; else $dis = null;
    $temp = explode('(',$value[1]);
    switch ($temp[0]) {
        case 'int':
            echo '<span>'.$value[0].' </span><input type="number" name="'.$value[0].'" type ="text" value="'.$inputval.'" '. $dis .'><br>';
            break;
        case 'varchar':
            echo '<span>'.$value[0].' </span><input type="text" name="'.$value[0].'" type ="text" value="'.$inputval.'" '. $dis .'><br>';
            break;
        case 'text':
            echo '<span>'.$value[0].' </span><input type="text" name="'.$value[0].'" type ="text" value="'.$inputval.'" '. $dis .'><br>';
            break;
        case 'date':
            echo '<span>'.$value[0].' </span><input type="date" name="'.$value[0].'" type ="text" value="'.$inputval.'" '. $dis .'><br>';
            break;
        case 'enum':
            echo '<span>'.$value[0].' </span><select name="'.$value[0].'" value="'.$inputval.'">';
            if ($inputval == 'active') {
                echo '<option value="active" selected>active</option><option value="passive">passive</option></select><br>';
            } else {
                echo '<option value="active" >active</option><option value="passive" selected>passive</option></select><br>';
            }
            break;
        case 'tinyint':
            echo '<span>'.$value[0].' </span><input type="number" name="'.$value[0].'" type ="text" value="'.$inputval.'" '. $dis .'><br>';
            break;
        case 'tinytext':
            echo '<span>'.$value[0].' </span><input type="file" accept="image/jpeg,image/png,image/gif" name="'.$value[0].'" type ="text" value="'.$inputval.'" '. $dis .'><br>';
            break;
        default:
            echo '<span>'.$value[0].' </span><input type="text" name="'.$value[0].'" type ="text" value="'.$inputval.'" '. $dis .'><br>';
            break;
    }
}

    echo '<input type="submit" value="Сохранить "></form>';



echo '<a href="showtable.php?table='.$table.'">Назад </a>';
echo '<a href="index.php"> На главную</a>';

