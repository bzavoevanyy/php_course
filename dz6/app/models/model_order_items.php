<?php
Class Model_order_items extends Model
{
    protected $table = 'order_items';
    public $timestamps = false;
    protected $fillable = ['id_order', 'id_product', 'price', 'count'];
}