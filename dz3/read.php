<!--Включаем сессию-->
<?php
session_start();
?>
<!doctype html>
<html lang="ru-RU">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/style.css">
    <title>Document</title>
</head>
<body>
<div class="wrapper">
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">TheBest CMS</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
<!--            Скрипт проеверяет - если пользователь авторизирован, выводит имя пользователя и кнопку "Выйти",-->
<!--            если нет - кнопку "Войти"-->
                <?php
                if (isset($_SESSION['name'])) {
                    echo '<p class="navbar-text">Signed in as '.$_SESSION["name"].'</p>
                <a href="logout.php" type="button" class="btn btn-default navbar-btn">Sign out</a>';
                } else {
                    echo '<a href="login.php" type="button" class="btn btn-default navbar-btn">Sign in</a>';
                }
                ?>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
<!--    В следующем скрипте выводим сайдбар если пользователь авторизирован-->
    <?php
    echo '<div class="container-fluid"><div class="row">';
    if (isset($_SESSION['name'])) {
        echo '<div class="col-md-2">
            <ul class="nav nav-pills nav-stacked">
                <li role="presentation"><a href="edit.php?flag=0">Создать файл</a></li>
                <li role="presentation"><a href="uploadform.php">Загрузить файл</a></li>
            </ul>
        </div>';};
    ?>
<!--    Скрипт вывовода содержимого файла -->
    <?php
    $file = $_GET['filename'];
    $dir = 'files/'.$file;
    $finfo = finfo_open(FILEINFO_MIME_TYPE); // Для определения mime типа файла
    $mime = finfo_file($finfo, $dir);
    if (!isset($_SESSION['name'])) {
        echo '<div class="container"><div class="row"><p>Вы не авторизованы</p><a class="btn btn-default" href="index.php">На главную</a></div></div>';
        session_destroy();
    } else {
//        Если пользователь авторизован - выводим содержимое файла
        echo '<div class="col-md-10">';
        if ($finfo) {
            if ($mime == 'text/plain') { // если текстовый файл
                echo '<p>';
                nl2br(readfile($dir));
                echo '</p>';
            } elseif ($mime == 'image/gif' || $mime == 'image/png' || $mime == 'image/jpeg') { // если картинка
                echo '<p><img class="img-rounded" src="' . $dir . '"></p>';
            } else echo 'Неправильный тип файла!';
        } else echo 'Ошибка! Не могу прочитать файл.';
        finfo_close($finfo);
        echo '<a class="btn btn-default" href="index.php">На главную</a>';
        echo '</div>';

    }
    ?>
</div>
</div>
<div class="empty"></div>
</div>
<footer>
    <div class="container">
        <div class="row">
            <span class="footer">&#169 The Best CMS. Bogdan Zavoevanyy LS PHP 02-16</span>
        </div>
    </div>
</footer>
</body>
</html>

