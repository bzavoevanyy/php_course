<!--Скрипт авторизации, в зависсимости от правильности логина и пароля записываем сообщение в переменную $str-->
<?php
session_start();
$login = $_POST['login'];
$password = $_POST['password'];
if ($login == 'demo') {
    if ($password == 'demo') {
        $_SESSION['name'] = 'demo';
        $str = '<p>Вы успешно вошли</p>';
    } else {
        $str = '<p>Неверный пароль</p>';
        session_destroy();
    }
} else {
    $str = '<p>Такого пользователя не существует</p>';
    session_destroy();
}
?>
<!doctype html>
<html lang="ru-RU" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/style.css">
    <title>Document</title>
</head>
<body>
<div class="wrapper">
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">TheBest CMS</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <!--            Скрипт проеверяет - если пользователь авторизирован, выводит имя пользователя и кнопку "Выйти",-->
                <!--            если нет - кнопку "Войти"-->
                <?php
                if (isset($_SESSION['name'])) {
                    echo '<p class="navbar-text">Signed in as '.$_SESSION["name"].'</p>
            <a href="logout.php" type="button" class="btn btn-default navbar-btn">Sign out</a>';
                } else {
                    echo '<a href="login.php" type="button" class="btn btn-default navbar-btn">Sign in</a>';
                }
                ?>
</div><!-- /.navbar-collapse -->
</div><!-- /.container-fluid -->
</nav>
<div class="container">
    <div class="row">
        <div class="col-md-6">
<!--            Выводим сообщение из переменной $str-->
            <?php
            echo $str;
            echo '<a class = "btn btn-default" href="index.php">На главную</a>';
            ?>
        </div>
    </div>
</div>

<div class="empty"></div>
</div></div>
<footer>
    <div class="container">
        <div class="row">
            <span class="footer">&#169 The Best CMS. Bogdan Zavoevanyy LS PHP 02-16</span>
        </div>
    </div>
</footer>
</body>
</html>


