<?php

class Controller_export extends Controller
{
    function action_index()
    {
        include 'app/core/map.php';
        switch ($_POST['table']) {
            case 'category':
                $this->model = new Model_category();
                break;
            case 'products':
                $this->model = new Model_products();
                break;
            case 'users':
                $this->model = new Model_users();
                break;
            case 'orders':
                $this->model = new Model_orders();
                break;
            case 'order_items':
                $this->model = new Model_order_items();
                break;

        }
        if ($_POST['type'] == 'csv') {
            $temp = $this->model->all()->toArray();
            $handler = fopen('temp/'.$_POST["table"].'.csv', 'w+');
            $ar = null;
            foreach ($map[$_POST['table']] as $key => $value) {
                $ar[] = $key;
            }
            fputcsv($handler, $ar, ';', '"');
            foreach ($temp as $key => $value) {
                fputcsv($handler, $value, ';', '"');
            }

        }
        if ($_POST['type'] == 'json') {
            $temp = $this->model->all()->toJson();
            $handler = fopen('temp/'.$_POST["table"].'.json', 'w+');
            fwrite($handler, $temp);
        }
        if ($_POST['type'] == 'xml') {
            $temp = $this->model->all()->toArray();
            $dom = new DOMDocument('1.0', 'UTF-8');
            if ($_POST['table'] == 'category') {
                $node = 'categories';
            } else $node = $_POST['table'];
            $tab = $dom->appendChild($dom->createElement($node));
            foreach ($temp as $key => $value) {
                $temp2 = $tab->appendChild($dom->createElement(substr($node, 0, -1)));
                foreach ($value as $k => $val) {
                    $temp3 = $temp2->appendChild($dom->createElement($k));
                    $temp3->appendChild($dom->createTextNode($val));
                }
            }
            $dom->saveXML();
            $dom->save('temp/'.$_POST["table"].'.xml');
        }
        $this->view->generate('export.html', $_POST['table'],null,[1,1,1,1],$_POST['type']);
    }
}