<?php

/* category.html */
class __TwigTemplate_1003a4a3b7f533d8ca1cf00d50ed10f365b2c96654fa9338da521ece002ca88e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("index.html", "category.html", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'menu' => array($this, 'block_menu'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "index.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        echo "Категории товаров";
    }

    // line 3
    public function block_menu($context, array $blocks = array())
    {
        // line 4
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["mainmenu"]) ? $context["mainmenu"] : null));
        foreach ($context['_seq'] as $context["key"] => $context["value"]) {
            // line 5
            if (($context["key"] == "Категории")) {
                // line 6
                echo "<li role=\"presentation\" class=\"active\">
    <a href=\"";
                // line 7
                echo twig_escape_filter($this->env, $context["value"], "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $context["key"], "html", null, true);
                echo "</a>
</li>
";
            }
            // line 10
            if (($context["key"] != "Категории")) {
                // line 11
                echo "<li role=\"presentation\">
    <a href=\"";
                // line 12
                echo twig_escape_filter($this->env, $context["value"], "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $context["key"], "html", null, true);
                echo "</a>
</li>
";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['value'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
    }

    // line 17
    public function block_content($context, array $blocks = array())
    {
        // line 18
        $this->loadTemplate("content.html", "category.html", 18)->display($context);
    }

    public function getTemplateName()
    {
        return "category.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 18,  75 => 17,  61 => 12,  58 => 11,  56 => 10,  48 => 7,  45 => 6,  43 => 5,  39 => 4,  36 => 3,  30 => 2,  11 => 1,);
    }
}
/* {%extends 'index.html'%}*/
/* {%block title%}Категории товаров{%endblock%}*/
/* {%block menu%}*/
/* {% for key, value in mainmenu %}*/
/* {% if key == 'Категории' %}*/
/* <li role="presentation" class="active">*/
/*     <a href="{{value}}">{{key}}</a>*/
/* </li>*/
/* {% endif %}*/
/* {% if key != 'Категории' %}*/
/* <li role="presentation">*/
/*     <a href="{{value}}">{{key}}</a>*/
/* </li>*/
/* {% endif %}*/
/* {% endfor %}*/
/* {%endblock%}*/
/* {%block content%}*/
/* {%include 'content.html'%}*/
/* {%endblock%}*/
