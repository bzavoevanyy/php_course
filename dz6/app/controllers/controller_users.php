<?php

class Controller_users extends Controller
{
    private $table = 'users';


    function action_index()
    {
        $this->model = new Model_users();
        $data = $this->model->all()->toArray();
        $this->view->generate('users.html', $this->table, $data);

    }

    function action_edit()
    {
        echo 'edit';
    }

    function action_findone($param)
    {
        $this->model = new Model_users();
        $data[0] = $this->model->find($param[0])->toArray();
        $this->view->generate('users.html', $this->table, $data, [0, 1, 1, 0]);
    }

    function action_search()
    {
        $this->model = new Model_users();
        if (!empty($_POST)) {
            if ($_POST['field'] == 'id') {
                $data[0] = $this->model->find($_POST['value'])->toArray();
            } else {
                $data = $this->model->where($_POST['field'], '=', $_POST['value'])->get()->toArray();
            }
        }
        if (count($data) > 1) $access = [1, 1, 1, 0]; else $access = [0, 1, 1, 0];
        $this->view->generate('users.html', $this->table, $data, $access);
    }

    function action_update()
    {
        $this->model = new Model_users();
        if (!empty($_POST)) {
            $temp = $this->model->find($_POST['id']);
            $temp->$_POST['field'] = $_POST['value'];
            $temp->save();
        }
        $this->action_index();
    }

    function action_create()
    {
        $this->model = new Model_users();
        $this->model->title = $_POST['title'];
        $this->model->status = $_POST['status'];
        $this->model->save();
        $this->action_index();
    }
    function action_delete($param)
    {
        $this->model = new Model_users();
        $temp = $this->model->find($param[0]);
        $temp->delete();
        $this->action_index();
    }
}