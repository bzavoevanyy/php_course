<?php

function vkRequest($method, $options = array(), $token = ''){
$params = http_build_query($options);
    $url = 'https://api.vk.com/method/'.$method.'?'.$params.'&access_token='.$token;
    $curl = curl_init(); // Начинаем построение curl запроса

    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); // Запрещаем вывод в браузер
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true); // Убираем проверку SSL
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false); // Убираем SSL проверку хоста
    curl_setopt($curl, CURLOPT_URL, $url); // Устанавливаем URL для запроса

    $response = curl_exec($curl);
    curl_close($curl);
    return $response;
}

echo '<a href="index.php">На главную</a><br>';

function getCountries() {

		$temp = vkRequest('database.getCountries',array('need_all'=>0));
		$countries = (json_decode($temp)->response);
		foreach ($countries as $key => $value) {
			
				$html .= '<a href="index.php?action=region&country='.$value->cid.'">'.$value->title.'</a><br>';
		}
		echo $html;
}
function getRegions($country_id) {
		$temp = vkRequest('database.getRegions',array('country_id'=>$country_id,'count'=>250));
		$countries = (json_decode($temp)->response);
		if (empty($countries)) {
			getCity($country_id);
		} else {
			foreach ($countries as $key => $value) {
			
				$html .= '<a href="index.php?action=city&country='.$country_id.'&region='.$value->region_id.'">'.$value->title.'</a><br>';
		}
		echo $html;
		}
}
function getCity($country_id, $region ='') {
		$temp = vkRequest('database.getCities',array('country_id'=>$country_id, 'region_id' => $region, 'count'=>250));
		$countries = (json_decode($temp)->response);
		foreach ($countries as $key => $value) {
			
				$html .= '<a href="index.php?action=school&city_id='.$value->cid.'">'.$value->title.'</a><br>';
		}
		echo $html;
}
function getSchool($city_id) {
		$temp = vkRequest('database.getSchools',array('city_id'=>$city_id, 'count'=>250));
		$countries = (json_decode($temp)->response);
		foreach ($countries as $key => $value) {
			
				$html .= $value->title.'<br>';
		}
		echo $html;
}

$action = $_GET['action'];
switch ($action) {
	case '':
		getCountries();
		break;
	case 'region':
		$country_id = $_GET['country'];
		getRegions($country_id);
		break;
	case 'city':
		$country_id = $_GET['country'];
		$region = $_GET['region'];
		getCity($country_id,$region);
		break;
	case 'school':
		$city_id = $_GET['city_id'];
		getSchool($city_id);
		break;
}
